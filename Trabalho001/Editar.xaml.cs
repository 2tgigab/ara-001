﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Trabalho001
{
	/// <summary>
	/// Interaction logic for Editar.xaml
	/// </summary>
	public partial class Editar : Window
	{
		//Id do User
		static int idUser;
		static int idContratoPub;

		//Instanciar conexao
		Connection ligar = new Connection();

		public Editar(int id , int idContrato)
		{
			InitializeComponent();
			comboNome.Visibility = Visibility.Hidden;
			idUser = id;
			idContratoPub = idContrato;
			EditarBD();
		}

		private void btnVoltar_Click(object sender, RoutedEventArgs e)
		{
			PagAdmin w = new PagAdmin(idUser);
			w.Show();
			this.Close();
		}

		private void EditarBD()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand($"UPDATE tblcontrato SET idFuncionario ='{idUser}', nomeFuncionario ='{comboNome.Items[0].ToString()}',diaMarcado ='{PegarDia()}' WHERE idContrato ={idContratoPub}", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Executar comando
			mySqlCommand.ExecuteReader();


			//Enviar Email
			string mensagem = $"O funcionário {comboNome.Items[0].ToString()} ({idUser}) Atualizou com sucesso o Contrato com o ID - {idContratoPub} no dia {PegarDia()}.";
			EnviarEmail("a256026742@aeaquaalba.pt", "teste123@mailinator.com", "Atualização Contrato", mensagem);
		}

		#region Pegar Dia

		private string PegarDia()
		{
			//Pegar dia de hoje
			string data = DateTime.Now.ToString("dd-MM-yyyy");

			return data;
		}
		#endregion

		public void PegarNome()
		{
			comboNome.Items.Clear();

			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT userName FROM tbluser WHERE idUser =" + idUser + ";", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				comboNome.Items.Add(reader.GetString(0).ToString());
			}
			//Fechar conexao com base de dados
			mySqlConnection.Close();

		}

		#region Eviar email passaword
		/// <summary>
		/// Metedo que esta responsavel por manda email
		/// </summary>
		/// <param name="remetente"></param>
		/// <param name="para"></param>
		/// <param name="asssuto"></param>
		/// <param name="mensaguem"></param>
		/// <returns></returns>
		public void EnviarEmail(string remetente, string para, string asssuto, string mensaguem)
		{
			//decalraçãop  de var
			//mailMessage- recebe os valores para mandar o email
			//client-abre uma conecçãoapra mandar o email
			MailMessage mailMessage = new MailMessage();
			SmtpClient client = new SmtpClient("smtp-relay.sendinblue.com");

			//bloco que esta responsaver por mandar todos os tipo de infos
			mailMessage.From = new MailAddress(remetente);
			mailMessage.To.Add(para);
			mailMessage.Subject = asssuto;
			mailMessage.Body = mensaguem;

			//Abre uma conecçãoa para manda o email
			client.Port = 587;
			client.Credentials = new NetworkCredential("rauldiogoms@gmail.com", "3VMtGcgOQpnv0rIa");
			client.EnableSsl = true;

			//Manda o emais
			client.Send(mailMessage);

		}
		#endregion

	}
}
