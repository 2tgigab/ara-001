﻿using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace Trabalho001
{
	/// <summary>
	/// Interaction logic for ArquivarContrato.xaml
	/// </summary>
	public partial class ArquivarContrato : Window
	{
		//id do utilizador
		static int idUser;

		//Caminho PDF
		static string file;
		static string caminho;

		
		

		//Instanciar conexao
		Connection ligar = new Connection();

		public ArquivarContrato(int id)
		{
			InitializeComponent();

			#region ANIMAÇÃO

			//Animação

			imgC1.Visibility = Visibility.Hidden;
			butFile.Visibility = Visibility.Hidden;
			imgC2.Visibility = Visibility.Hidden;
			imgErrado2.Visibility = Visibility.Hidden;
			imgC3.Visibility = Visibility.Hidden;
			imgE3.Visibility = Visibility.Hidden;
			finalBut.Visibility = Visibility.Hidden;
			comboNome.Visibility = Visibility.Hidden;
			comboIdFun.Visibility = Visibility.Hidden;
			comboIdContrato.Visibility = Visibility.Hidden;
			butPath.Visibility = Visibility.Hidden;
			//lblPath.Visibility = Visibility.Hidden;

			#endregion

			idUser = id;
		}

		private void butFile_Click(object sender, RoutedEventArgs e)
		{
			Microsoft.Win32.OpenFileDialog openFile = new Microsoft.Win32.OpenFileDialog();
			bool? response = openFile.ShowDialog();

			if (response == true)
			{

				//String para pegar o tipo de ficheiro (PDF no caso)
				string nomeFicheiro = openFile.FileName;
				 file = openFile.FileName;

				//Tamanho do ficheiro
				int tamanho = nomeFicheiro.Length - 4;

				nomeFicheiro = nomeFicheiro.Substring(tamanho, 4);

				//Se o ficheiro nao for PDF mostrar erro
				if (nomeFicheiro != ".pdf")
				{
					System.Windows.MessageBox.Show("Insira um ficheiro no formato PDF!", "ERRO!", MessageBoxButton.OK, MessageBoxImage.Error);
					imgC2.Visibility = Visibility.Hidden;
				}
				else
				{
					imgErrado2.Visibility = Visibility.Hidden;
					imgC2.Visibility = Visibility.Visible;
					butPath.Visibility = Visibility.Visible;
					imgE3.Visibility = Visibility.Visible;
				}
			}


		}

		private void btnVoltar_Click(object sender, RoutedEventArgs e)
		{
			Funcionario w = new Funcionario(idUser);
			w.Show();
			this.Close();
		}

		private void txbIdContrato_LostFocus(object sender, RoutedEventArgs e)
		{
			//Verificar se a textbox está vazia
			if (txbIdContrato.Text == "")
			{
				txbIdContrato.Text = "Insira ID do Contrato";
			}
		}

		private void txbIdContrato_GotFocus(object sender, RoutedEventArgs e)
		{
			if (txbIdContrato.Text == "Insira ID do Contrato")
			{
				txbIdContrato.Text = "";
			}
		}

		private void txbIdContrato_TextChanged(object sender, TextChangedEventArgs e)
		{
			//Verificar se String contem letras
			if (txbIdContrato.Text.All(char.IsLetter) || txbIdContrato.Text == "")
			{
				imgC1.Visibility = Visibility.Hidden;
				imgE1.Visibility = Visibility.Visible;
				butFile.Visibility = Visibility.Hidden;
			}
			else
			{
				imgC1.Visibility = Visibility.Visible;
				imgE1.Visibility = Visibility.Hidden;
				butFile.Visibility = Visibility.Visible;

			}

		}

		private void butPath_Click(object sender, RoutedEventArgs e)
		{
			var dialog = new System.Windows.Forms.FolderBrowserDialog();
			System.Windows.Forms.DialogResult result = dialog.ShowDialog();
			lblPath.Content = dialog.SelectedPath;
			caminho = dialog.SelectedPath;
			caminho = caminho + $@"\Contrato_{txbIdContrato.Text}.pdf";
			finalBut.Visibility = Visibility.Visible;
			imgC3.Visibility = Visibility.Visible;
			imgE3.Visibility = Visibility.Hidden;
		}

		private void finalBut_Click(object sender, RoutedEventArgs e)
		{
			comboNome.Items.Clear();
			PegarNome();
			//Quando clicar no botao para arquivar, verificar se ja existe um contrato com aquele ID
			//Se ja existir, pedir para atualizar senao, criar um novo
			verificarExiste();
			//Verificar se o id do contrato ja existe
			ArquivarArquivo();

			
		}

		#region Arquivos


		/// <summary>
		/// Vai verificar se o Arquivo existe
		/// </summary>
		private void verificarExiste()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT idContrato FROM tblcontrato WHERE idContrato =" + txbIdContrato.Text + ";", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				comboIdContrato.Items.Add(reader.GetString(0).ToString());
			}
			//Fechar conexao com base de dados
			mySqlConnection.Close();
		}

		private void AtualizarArquivo()
		{
			comboIdFun.Items.Clear();

			//Verificar se Pode atualizar (Atraves do ID)
			PegarIdFun();

			if (comboIdFun.Items[0].ToString() == idUser.ToString())
			{
				//Estabelecer conexão com a base de dados
				MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);


				//Comando para dar Select
				MySqlCommand mySqlCommand = new MySqlCommand($"UPDATE tblcontrato SET idFuncionario ='{idUser}', nomeFuncionario ='{comboNome.Items[0].ToString()}',diaMarcado ='{PegarDia()}' WHERE idContrato ={txbIdContrato.Text}", mySqlConnection);

				//Abrir Conexão
				mySqlConnection.Open();

				//Executar comando
				mySqlCommand.ExecuteReader();

				//Guardar no PC
				File.Move(file, caminho);

				//Enviar Email
				string mensagem = $"O funcionário {comboNome.Items[0].ToString()} ({idUser}) Atualizou com sucesso o Contrato com o ID - {txbIdContrato.Text} no dia {PegarDia()}.";
				EnviarEmail("a256026742@aeaquaalba.pt", "teste123@mailinator.com", "Atualização Contrato", mensagem);
			}
			else
			{
				System.Windows.MessageBox.Show("NÃO PODES EDITAR ESTE CONTRATO", "ERRO", MessageBoxButton.OK , MessageBoxImage.Error);
			}


		}

		public void ArquivarArquivo()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			try
			{
				//Comando para dar Select
				MySqlCommand mySqlCommand = new MySqlCommand($"INSERT INTO tblcontrato(idContrato,idFuncionario,nomeFuncionario,diaMarcado) VALUES ('{txbIdContrato.Text}','{idUser}','{comboNome.Items[0].ToString()}','{PegarDia()}');", mySqlConnection);

				//Abrir Conexão
				mySqlConnection.Open();

				//Executar comando
				MySqlDataReader reader = mySqlCommand.ExecuteReader();

				//Arquivar no PC
				File.Move(file, caminho);

				//Enviar Email
				string mensagem = $"O funcionário {comboNome.Items[0].ToString()} ({idUser}) inseriu com sucesso o Contrato com o ID - {txbIdContrato.Text} no dia {PegarDia()}.";
				EnviarEmail("a256026742@aeaquaalba.pt", "teste123@mailinator.com", "Atualização Contrato", mensagem);
			}
			catch
			{
				//Mostar que o arquivo ja existe e perguntar se tenciona a dar update


				DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Ficheiro já existe." +
					"Pretende atualizar?", "Some Title", MessageBoxButtons.YesNo);
				if (dialogResult == System.Windows.Forms.DialogResult.Yes)
				{
					AtualizarArquivo();
				}
				
			}

		}

		public void PegarIdFun()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT idFuncionario FROM tblcontrato WHERE idContrato =" + txbIdContrato.Text + ";", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				comboIdFun.Items.Add(reader.GetString(0).ToString());
			}
			//Fechar conexao com base de dados
			mySqlConnection.Close();

		}

		/// <summary>
		/// Irá pegar o nome do funcionario que esta a arquivar
		/// </summary>
		public void PegarNome()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT userName FROM tbluser WHERE idUser =" + idUser + ";", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				comboNome.Items.Add(reader.GetString(0).ToString());
			}
			//Fechar conexao com base de dados
			mySqlConnection.Close();

		}

		/// <summary>
		/// Pegar o dia em que o arquivo foi arquivado
		/// </summary>
		private string PegarDia()
		{
			//Pegar dia de hoje
			string data = DateTime.Now.ToString("dd-MM-yyyy");

			return data;
		}

		#endregion

		#region Eviar email passaword
		/// <summary>
		/// Metedo que esta responsavel por manda email
		/// </summary>
		/// <param name="remetente"></param>
		/// <param name="para"></param>
		/// <param name="asssuto"></param>
		/// <param name="mensaguem"></param>
		/// <returns></returns>
		public void EnviarEmail(string remetente, string para, string asssuto, string mensaguem)
		{
			//decalraçãop  de var
			//mailMessage- recebe os valores para mandar o email
			//client-abre uma conecçãoapra mandar o email
			MailMessage mailMessage = new MailMessage();
			SmtpClient client = new SmtpClient("smtp-relay.sendinblue.com");

			//bloco que esta responsaver por mandar todos os tipo de infos
			mailMessage.From = new MailAddress(remetente);
			mailMessage.To.Add(para);
			mailMessage.Subject = asssuto;
			mailMessage.Body = mensaguem;

			//Abre uma conecçãoa para manda o email
			client.Port = 587;
			client.Credentials = new NetworkCredential("rauldiogoms@gmail.com", "3VMtGcgOQpnv0rIa");
			client.EnableSsl = true;

			//Manda o emais
			client.Send(mailMessage);

		}
		#endregion

	}
}
