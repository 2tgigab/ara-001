CREATE TABLE `trabalho001`.`tbluser` (
  `idUser` INT NOT NULL AUTO_INCREMENT COMMENT 'Id do utilizador com auto_increment para nao haverem ids repetidos',
  `userName` VARCHAR(255) NOT NULL COMMENT 'Campo para pegar o Nome do User',
  `userPw` VARCHAR(255) NOT NULL COMMENT 'Campo para pegar a Password do User',
  `userCargo` VARCHAR(255) NOT NULL COMMENT 'Campo para pegar o cargo do User (A - Admin F- Funcionario)',
  `userEmail` VARCHAR(255) NOT NULL COMMENT 'Campo para pegar o Nome do User',
  PRIMARY KEY (`idUser`))
COMMENT = 'Tabela para Login';

CREATE TABLE `trabalho001`.`tblcontrato` (
  `idContrato` INT NOT NULL COMMENT 'Id Do Contrato',
  `idFuncionario` VARCHAR(255) NOT NULL COMMENT 'Id do Funcionario responsavel por arquivar o PDF',
  `nomeFuncionario` VARCHAR(255) NOT NULL COMMENT 'Id do Funcionario responsavel por arquivar o PDF',
  `diaMarcado` VARCHAR(45) NOT NULL COMMENT 'Dia em que o Ficheiro PDF é Arquivado',
  PRIMARY KEY (`idContrato`))
COMMENT = 'Tabela Contratos';

INSERT INTO tblcontrato(idContrato,idFuncionario,nomeFuncionario,diaMarcado) VALUES ('1','1','1','1');

SELECT userCargo FROM tbluser WHERE idUser = 3;

SELECT * FROM tbluser;
SELECT * FROM tblcontrato;

UPDATE tblcontrato SET idFuncionario = 3 , nomeFuncionario ='{comboNome.Items[0].ToString()},diaMarcado ='{PegarDia()}', WHERE idContrato ={txbIdContrato.Text}

INSERT INTO tbluser(userName,userPw,userCargo,userEmail) VALUES ('Gabriel Neto','tgpsi','A','gabrielneto@gmail.com');

INSERT INTO tbluser(userName,userPw,userCargo,userEmail) VALUES ('Boneco Teste','tgpsi','F','BonecoTeste@gmail.com');
INSERT INTO tbluser(userName,userPw,userCargo,userEmail) VALUES ('1','1','F','1');
