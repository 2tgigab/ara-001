﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trabalho001
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		//Instanciar objeto de ligação a base de dados
		static Connection ligar = new Connection();

		//Id do utilizador que vai dar login
		static int idUser;

		public MainWindow()
		{
			InitializeComponent();

			#region Colocar ComboBoxes Invisiveis
			//Colocar ComboBoxes invisiveis
			ComboVerificar.Visibility = Visibility.Hidden;
			PassCombo.Visibility = Visibility.Hidden;
			idCombo.Visibility = Visibility.Hidden;
			NomeCombo.Visibility = Visibility.Hidden;
			CargoCombo.Visibility = Visibility.Hidden;
			#endregion

		}

		#region GET AND LOST FOCUS


		private void txbUser_GotFocus(object sender, RoutedEventArgs e)
		{
			if(txbUser.Text == "Username")
			{
				txbUser.Text = "";
			}

		}

		private void txbUser_LostFocus(object sender, RoutedEventArgs e)
		{
			//Verificar se a textbox está vazia
			if (txbUser.Text == "")
			{
				txbUser.Text = "Username";
			}
		}

		private void txbPass_LostFocus(object sender, RoutedEventArgs e)
		{
			//Verificar se a textbox está vazia
			if (txbPass.Text == "")
			{
				txbPass.Text = "Password";
			}
		}

		private void txbPass_GotFocus(object sender, RoutedEventArgs e)
		{
			if(txbPass.Text == "Password")
			{
				txbPass.Text = "";
			}
		}

		private void txbEmail_GotFocus(object sender, RoutedEventArgs e)
		{
			if(txbEmail.Text == "E-mail")
			{
				txbEmail.Text = "";
			}
		}

		private void txbEmail_LostFocus(object sender, RoutedEventArgs e)
		{
			//Verificar se a textbox está vazia
			if (txbEmail.Text == "")
			{
				txbEmail.Text = "E-mail";
			}
		}

		#endregion

		#region Botão Login


		/// <summary>
		/// Metodo para quando clicar em Login
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLogin_Click(object sender, RoutedEventArgs e)
		{

			//Limpar items da combobox
			ComboVerificar.Items.Clear();
			idCombo.Items.Clear();
			NomeCombo.Items.Clear();
			PassCombo.Items.Clear();
			CargoCombo.Items.Clear();


			//Se ao clicar no botao login, as cardenciais estiverem certas efetuar login
			//Se algum erro acontecer, apresentá-lo

			if (VerificarExiste() == true)
			{
				PegarID();

				//Pegar id inserido na comboBox
				idUser = int.Parse(idCombo.Items[0].ToString());

				//Metodo para verificar se a password esta correta
				VerificarPassword();

				//Verificar se Username esta correto
				VerificarUser();

				//Verificar tudo, agora de acordo com as textboxes

				//Verificar username
				if(NomeCombo.Items[0].ToString() == txbUser.Text)
				{
					//Verificar a senha
					if(PassCombo.Items[0].ToString() == txbPass.Text)
					{
						//Verificar Cargo
						VerificarCargo();

						//Se o cargo for F, abrir menu funcionario
						//Se for A, abrir Menu Admin
						if(CargoCombo.Items[0].ToString() == "F")
						{

							//Instanciar Objeto User para guardar os dados do utilizador por tempo limitado
							PegarUser pegar = new PegarUser();
							pegar.userNamePub = txbUser.Text;
							pegar.userEmailPub = txbEmail.Text;
							pegar.userCargoPub = CargoCombo.Items[0].ToString();

							//Abrir Menu Funcionario

							Funcionario w = new Funcionario(idUser);
							w.Show();
							this.Close();
						}
						else
						{
							Funcionario w = new Funcionario(idUser);
							w.Show();
							this.Close();
						}
					}
					else
					{
						//Mostrar Mensagem de Erro
						MessageBox.Show("ERRO", "SENHA ERRADA!", MessageBoxButton.OK, MessageBoxImage.Error);

					}
				}
				else
				{
					//Mostrar Mensagem de erro
					MessageBox.Show("ERRO", "User Errado!", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
		}

		#endregion

		#region Verificações

		/// <summary>
		/// Verificar se pessoa ja existe
		/// </summary>
		private bool VerificarExiste()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT userEmail FROM tbluser WHERE userEmail ='" + txbEmail.Text + "';", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();
			
				//Leitor vai ser igual ao que ele executar
				MySqlDataReader reader = mySqlCommand.ExecuteReader();

				//Enquanto estiver a ler manda tudo para a combobox
				while (reader.Read())
				{
					//Manda para a combo box
					ComboVerificar.Items.Add(reader.GetString(0).ToString());
				}
				//Fechar conexao com base de dados
				mySqlConnection.Close();

			string email = "";
			try
			{
				email = ComboVerificar.Items[0].ToString();

			}
			catch
			{
				email = "";
			}

			if (email == "")
			{
				MessageBox.Show("ERRO", "User Não Existe", MessageBoxButton.OK, MessageBoxImage.Error);
				//Fechar conexao com base de dados
				mySqlConnection.Close();
				return false;
			}
			else
			{
				return true;
			}
		}

		private void PegarID()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT idUser FROM tbluser WHERE userEmail ='" + txbEmail.Text + "';", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				idCombo.Items.Add(reader.GetString(0).ToString());
			}

			

			//Fechar conexao com base de dados
			mySqlConnection.Close();
		}

		private void VerificarPassword()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT userPW FROM tbluser WHERE idUser =" + idUser + ";", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				PassCombo.Items.Add(reader.GetString(0).ToString());
			}

			//Fechar conexao com base de dados
			mySqlConnection.Close();

		}

		

		private void VerificarUser()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT userName FROM tbluser WHERE idUser =" + idUser + ";", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				NomeCombo.Items.Add(reader.GetString(0).ToString());
			}

			//Fechar conexao com base de dados
			mySqlConnection.Close();

		}

		private void VerificarCargo()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT userCargo FROM tbluser WHERE idUser =" + idUser + ";", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				CargoCombo.Items.Add(reader.GetString(0).ToString());
			}

			//Fechar conexao com base de dados
			mySqlConnection.Close();

		}

		#endregion

	}
}
