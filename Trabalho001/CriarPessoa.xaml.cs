﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Trabalho001
{
	/// <summary>
	/// Interaction logic for CriarPessoa.xaml
	/// </summary>
	public partial class CriarPessoa : Window
	{

		//Id do User
		static int idUser;

		//Instanciar conexao
		Connection ligar = new Connection();

		public CriarPessoa(int id)
		{
			InitializeComponent();
			idUser = id;
		}

		private void btnVoltar_Click(object sender, RoutedEventArgs e)
		{
			PagAdmin w = new PagAdmin(idUser);
			w.Show();
			this.Close();
		}

		private void AdicionarUser()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);


			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand($"INSERT INTO tbluser(userName,userPw,userCargo,userEmail) VALUES ('{txbUser.Text}','{txbPass.Text}','F','{txbEmail.Text}');", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Executar comando
			mySqlCommand.ExecuteReader();

			//Fechar conexao
			mySqlConnection.Close();
		}

		private void btnLogin_Click(object sender, RoutedEventArgs e)
		{
			AdicionarUser();

			//Enviar um email para o user criado com as informações
			string mensagem = $"Bem-Vindo, venho por este meio lhe fornecer as informações da sua conta.\n" +
				$"Username : {txbUser.Text}\n" +
				$"Password: {txbPass.Text}\n" +
				$"E-mail: {txbEmail.Text}\n" +
				$"Obrigado, se precisar de ajuda nao hesite em contactar.";
			EnviarEmail("a256026742@aeaquaalba.pt", txbEmail.Text , "Dados da Conta", mensagem);

			MessageBox.Show("Conta Criada", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
			PagAdmin w = new PagAdmin(idUser);
			w.Show();
			this.Close();

		}

		#region Eviar email passaword
		/// <summary>
		/// Metedo que esta responsavel por manda email
		/// </summary>
		/// <param name="remetente"></param>
		/// <param name="para"></param>
		/// <param name="asssuto"></param>
		/// <param name="mensaguem"></param>
		/// <returns></returns>
		public void EnviarEmail(string remetente, string para, string asssuto, string mensaguem)
		{
			//decalraçãop  de var
			//mailMessage- recebe os valores para mandar o email
			//client-abre uma conecçãoapra mandar o email
			MailMessage mailMessage = new MailMessage();
			SmtpClient client = new SmtpClient("smtp-relay.sendinblue.com");

			//bloco que esta responsaver por mandar todos os tipo de infos
			mailMessage.From = new MailAddress(remetente);
			mailMessage.To.Add(para);
			mailMessage.Subject = asssuto;
			mailMessage.Body = mensaguem;

			//Abre uma conecçãoa para manda o email
			client.Port = 587;
			client.Credentials = new NetworkCredential("rauldiogoms@gmail.com", "3VMtGcgOQpnv0rIa");
			client.EnableSsl = true;

			//Manda o emais
			client.Send(mailMessage);

		}
		#endregion

		#region GET AND LOST FOCUS


		private void txbUser_GotFocus(object sender, RoutedEventArgs e)
		{
			if (txbUser.Text == "Username")
			{
				txbUser.Text = "";
			}

		}

		private void txbUser_LostFocus(object sender, RoutedEventArgs e)
		{
			//Verificar se a textbox está vazia
			if (txbUser.Text == "")
			{
				txbUser.Text = "Username";
			}
		}

		private void txbPass_LostFocus(object sender, RoutedEventArgs e)
		{
			//Verificar se a textbox está vazia
			if (txbPass.Text == "")
			{
				txbPass.Text = "Password";
			}
		}

		private void txbPass_GotFocus(object sender, RoutedEventArgs e)
		{
			if (txbPass.Text == "Password")
			{
				txbPass.Text = "";
			}
		}

		private void txbEmail_GotFocus(object sender, RoutedEventArgs e)
		{
			if (txbEmail.Text == "E-mail")
			{
				txbEmail.Text = "";
			}
		}

		private void txbEmail_LostFocus(object sender, RoutedEventArgs e)
		{
			//Verificar se a textbox está vazia
			if (txbEmail.Text == "")
			{
				txbEmail.Text = "E-mail";
			}
		}

		#endregion
	}
}
