﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Trabalho001
{
	/// <summary>
	/// Interaction logic for Funcionario.xaml
	/// </summary>
	public partial class Funcionario : Window
	{
		static int idUser;

		//Instanciar objeto de ligação a base de dados
		static Connection ligar = new Connection();

		public Funcionario(int id)
		{
			InitializeComponent();

			

			

			//Animação
			ComboCargo.Visibility = Visibility.Hidden;

			//Pegar id do user iniciado
			idUser = id;

			//Iniciar metodo para verificar cargo
			PegarCargo();

			if(ComboCargo.Items[0].ToString() == "F")
			{
				butAdmin.Visibility = Visibility.Hidden;
			}


			//Pegar dados e inserir na DataGrid
			BaseDados();

		}

		private void PegarCargo()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT userCargo FROM tbluser WHERE idUser =" + idUser + ";", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Leitor vai ser igual ao que ele executar
			MySqlDataReader reader = mySqlCommand.ExecuteReader();

			//Enquanto estiver a ler manda tudo para a combobox
			while (reader.Read())
			{
				//Manda para a combo box
				ComboCargo.Items.Add(reader.GetString(0).ToString());
			}
			//Fechar conexao com base de dados
			mySqlConnection.Close();

			//if(ComboCargo.Items[0].ToString())
		}

		public void BaseDados()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT idContrato AS Contrato , nomeFuncionario AS Funcionario , idFuncionario AS IdFuncionario , diaMarcado as Data FROM tblcontrato;", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Instanciar DataTable
			DataTable data = new DataTable();

			//Conversão de elementos
			data.Load(mySqlCommand.ExecuteReader());

			//Datagrid tomara o valor da conversão dos elementos
			GridView1.DataContext = data;

			//Fechar Conexão
			mySqlConnection.Close();

			
		}

		

		private void btnArquivar_Click(object sender, RoutedEventArgs e)
		{
			//Abrir uma nova janela para arquivar um contrato
			ArquivarContrato w = new ArquivarContrato(idUser);
			w.Show();

			//Fechar Pagina Atual
			this.Close();


			
		}

		private void txbBarra_TextChanged(object sender, TextChangedEventArgs e)
		{
			if(txbBarra.Text == "")
			{
				BaseDados();
			}
			else
			{
				//Estabelecer conexão com a base de dados
				MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

				//Comando para dar Select
				MySqlCommand mySqlCommand = new MySqlCommand($"SELECT idContrato AS Contrato , nomeFuncionario AS Funcionario , idFuncionario AS Email , diaMarcado as Data FROM tblcontrato WHERE nomeFuncionario LIKE '{txbBarra.Text}%';", mySqlConnection);

				//Abrir Conexão
				mySqlConnection.Open();

				//Instanciar DataTable
				DataTable data = new DataTable();

				//Conversão de elementos
				data.Load(mySqlCommand.ExecuteReader());

				//Datagrid tomara o valor da conversão dos elementos
				GridView1.DataContext = data;

				//Fechar Conexão
				mySqlConnection.Close();
			}
			
		}

		private void butAdmin_Click(object sender, RoutedEventArgs e)
		{
			PagAdmin w = new PagAdmin(idUser);
			w.Show();
			this.Close();
		}

        private void GridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
