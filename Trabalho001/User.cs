﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho001
{
	class User
	{
		public static string emailChefe = "GabrielNetoCompany@mailinator.com";

		protected string userName; 
		protected string userEmail; 
		protected string userCargo; 
	}

	class PegarUser : User
	{
		public string userNamePub
		{
			get { return userName; }
			set { userName = value; }
		}

		public string userEmailPub
		{
			get { return userEmail; }
			set { userEmail = value; }
		}

		public string userCargoPub
		{
			get { return userCargo; }
			set { userCargo = value; }
		}
	}

}
