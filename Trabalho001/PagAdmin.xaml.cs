﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Trabalho001
{
	/// <summary>
	/// Interaction logic for PagAdmin.xaml
	/// </summary>
	public partial class PagAdmin : Window
	{
		//Id do admin
		static int idUser;

		//Instanciar conexao
		Connection ligar = new Connection();

		public PagAdmin(int id)
		{
			InitializeComponent();
			BaseDados();
			idUser = id;
		}

		public void BaseDados()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);

			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand(@"SELECT idContrato AS Contrato , nomeFuncionario AS Funcionario , idFuncionario AS IdFuncionario , diaMarcado as Data FROM tblcontrato;", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Instanciar DataTable
			DataTable data = new DataTable();

			//Conversão de elementos
			data.Load(mySqlCommand.ExecuteReader());

			//Datagrid tomara o valor da conversão dos elementos
			GridView1.DataContext = data;

			//Fechar Conexão
			mySqlConnection.Close();


		}

		private void txbUser_LostFocus(object sender, RoutedEventArgs e)
		{
			if (txbUser.Text == "")
			{
				txbUser.Text = "Insira ID";
			}
		}

		private void txbUser_GotFocus(object sender, RoutedEventArgs e)
		{
			if (txbUser.Text == "Insira ID")
			{
				txbUser.Text = "";
			}
		}

		private void butElim_Click(object sender, RoutedEventArgs e)
		{
			//Mostrar mensagem a perguntar se tem a certeza de elimianr
			DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Está a Eliminar um ficheiro." +
				"Pretende Continuar?", "AVISO!", MessageBoxButtons.YesNo);
			if (dialogResult == System.Windows.Forms.DialogResult.Yes)
			{
				EliminarArquivo();
				txbUser.Text = "Insira ID";
			}
		}

		private void EliminarArquivo()
		{
			//Estabelecer conexão com a base de dados
			MySqlConnection mySqlConnection = new MySqlConnection(ligar.connection);


			//Comando para dar Select
			MySqlCommand mySqlCommand = new MySqlCommand($"DELETE FROM tblcontrato WHERE idContrato ={txbUser.Text}", mySqlConnection);

			//Abrir Conexão
			mySqlConnection.Open();

			//Executar comando
			mySqlCommand.ExecuteReader();

			//Fechar conexao
			mySqlConnection.Close();

			//Atualizar DataGrid
			BaseDados();
		}

		private void btnVoltar_Click(object sender, RoutedEventArgs e)
		{
			Funcionario w = new Funcionario(idUser);
			w.Show();
			this.Close();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Editar w = new Editar(idUser,int.Parse(txbUser.Text));
			w.Show();
			this.Close();
		}

		private void butCriar_Click(object sender, RoutedEventArgs e)
		{
			CriarPessoa w = new CriarPessoa(idUser);
			w.Show();
			this.Close();
		}
	}
}
